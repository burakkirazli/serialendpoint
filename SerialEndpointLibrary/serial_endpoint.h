/*
 * serial_endpoint.h
 *
 *  Created on: Feb 12, 2020
 *      Author: burak.kirazli
 */

#ifndef SERIALENDPOINTLIBRARY_SERIAL_ENDPOINT_H_
#define SERIALENDPOINTLIBRARY_SERIAL_ENDPOINT_H_

#include "circular.h"

#define SEP_CONTAINER_SIZE 10

typedef struct _sep_t* sep_t;

typedef void (*sep_timeout_cb_t)(int available_rx_data_size);
typedef int (*sep_tx_requester_cb_t)(void* tx_requested_data, int size);

sep_t sep_create(void* rx_buffer, int rx_size,
							void* tx_buffer, int tx_size,
								sep_tx_requester_cb_t tx_requester_cb,
									sep_timeout_cb_t timeout_cb,
										int timeout_val);
void sep_container_reset(void);
void sep_container_tick(int inc_val);
int sep_get_timeout_val(sep_t sep);
void sep_push_rx_data(sep_t sep, void* data, int size);
int sep_get_current_tick_val(sep_t sep);
int sep_read(sep_t sep, void* buffer, int size);
int sep_write(sep_t sep, void* data, int size);
void sep_tx_completed(sep_t sep);

#endif /* SERIALENDPOINTLIBRARY_SERIAL_ENDPOINT_H_ */
