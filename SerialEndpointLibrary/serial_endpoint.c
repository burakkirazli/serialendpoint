/*
 * serial_endpoint.c
 *
 *  Created on: Feb 12, 2020
 *      Author: burak.kirazli
 */

#include "serial_endpoint.h"
#include "circular.h"
#include <string.h>

struct _sep_t
{
	circular_t rx_buffer;
	char* tx_buffer;
	int tx_buffer_size;
	int is_tx_completed;
	sep_tx_requester_cb_t requester_cb;
	sep_timeout_cb_t timeout_cb;
	int timeout_val;
	int tick_val;
};

static struct _sep_t container[SEP_CONTAINER_SIZE];
unsigned short idx = 0;

sep_t sep_create(void* rx_buffer, int rx_size,
							void* tx_buffer, int tx_size,
								sep_tx_requester_cb_t transmit_cb,
									sep_timeout_cb_t timeout_cb,
										int timeout_val)
{
	if(idx < SEP_CONTAINER_SIZE)
	{
		struct _sep_t* sep = &container[idx];
		sep->rx_buffer = circular_ctor(rx_buffer, rx_size);
		sep->tx_buffer = tx_buffer;
		sep->tx_buffer_size = tx_size;
		sep->is_tx_completed = 1;
		sep->requester_cb = transmit_cb;
		sep->timeout_cb = timeout_cb;
		sep->timeout_val = timeout_val;
		idx++;
		return &container[idx - 1];
	}

	return 0;
}

void sep_container_reset(void)
{
	memset(container, 0, sizeof(container));
	idx = 0;
}

void sep_container_tick(int tick_val)
{
	for(int i = 0; i < idx; i++)
	{
		struct _sep_t* sep = &container[i];

		if(sep->tick_val < sep->timeout_val)
		{
			sep->tick_val += tick_val;
			if(sep->tick_val == sep->timeout_val)
			{
				if(circular_bytes_available(sep->rx_buffer) > 0)
				{
					if(sep->timeout_cb)
					{
						sep->timeout_cb(circular_bytes_available(sep->rx_buffer));
					}
				}
			}
		}
	}
}

void sep_set_timeout_val(sep_t sep, int timo)
{
	sep->timeout_val = timo;
}

int sep_get_timeout_val(sep_t sep)
{
	return sep->timeout_val;
}

void sep_set_timeout_cb(sep_t sep, sep_timeout_cb_t cb)
{
	sep->timeout_cb = cb;
}

void sep_push_rx_data(sep_t sep, void* data, int size)
{
	sep->tick_val = 0;
	circular_insert(sep->rx_buffer, data, size);
}

int sep_get_current_tick_val(sep_t sep)
{
	return sep->tick_val;
}

int sep_read(sep_t sep, void* buffer, int size)
{
	return circular_receive(sep->rx_buffer, buffer, size);
}

int sep_write(sep_t sep, void* data, int size)
{
	//
	if(sep->is_tx_completed == 0)
		return 0;

	//reset flag
	sep->is_tx_completed = 0;

	//copy data to buffer
	memcpy(sep->tx_buffer, data, size);

	//
	return sep->requester_cb(sep->tx_buffer, size);
}

void sep_tx_completed(sep_t sep)
{
	sep->is_tx_completed = 1;
}
