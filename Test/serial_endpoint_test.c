/*
 * serial_endpoint_test.c
 *
 *  Created on: Feb 12, 2020
 *      Author: burak.kirazli
 */

#include "unity_fixture.h"
#include "serial_endpoint.h"
#include "circular.h"
#include <string.h>
#include <stdlib.h>

//
#define RX_BUF_SIZE 2048
#define TX_BUF_SIZE 2048

//
sep_t sep;

//
static int flag = 0;
static int size = 0;
void sep_timo_callback(int s)
{
	flag = 1;
	size = s;
}


//
TEST_GROUP(serial);

TEST_SETUP(serial)
{
	//
	static char rx_buf[RX_BUF_SIZE];
	static char tx_buf[TX_BUF_SIZE];

	//
	memset(rx_buf, 0, RX_BUF_SIZE);
	memset(tx_buf, 0, TX_BUF_SIZE);

	//craete sep
	extern int transmit_callback(void* data, int size);
	sep = sep_create(rx_buf,
					 RX_BUF_SIZE,
					 tx_buf,
					 TX_BUF_SIZE,
					 transmit_callback,
					 sep_timo_callback,
					 100
					 );
}

TEST_TEAR_DOWN(serial)
{
	sep_container_reset();
	circular_reset_container();
	sep = 0;
}

TEST(serial, test_create)
{
	//
	TEST_ASSERT_NOT_EQUAL(0, sep);
}

TEST(serial, test_set_timeout)
{
	TEST_ASSERT_EQUAL(100, sep_get_timeout_val(sep));
}

void make_sep_timeout(void)
{
	flag = 0;
	for(int i = 0; i < 10; i++)
		sep_container_tick(10);
}

TEST(serial, test_tick_for_empty_rx_circ)
{
	//tick sep container
	make_sep_timeout();

	//test
	TEST_ASSERT_EQUAL(0, flag);
}

int push_some_rx_data(void)
{
	//push some data
	char data[100];
	memset(data, 'x', 100);
	sep_push_rx_data(sep, data, 100);
	return 100;
}

TEST(serial, test_tick_for_non_empty_rx_circ)
{
	//
	push_some_rx_data();

	//
	make_sep_timeout();

	//
	TEST_ASSERT_EQUAL(1, flag);
	TEST_ASSERT_EQUAL(100, size);
}

TEST(serial, test_tick_after_timeout)
{
	//make timeout
	make_sep_timeout();

	//make it again
	make_sep_timeout();

	//
	TEST_ASSERT_EQUAL(100, sep_get_current_tick_val(sep));
}

TEST(serial, test_rx_data)
{
	//
	push_some_rx_data();

	//
	char buffer[100];
	int result = sep_read(sep, buffer, 100);
	TEST_ASSERT_EQUAL(100, result);
	char some_data[100];
	memset(some_data, 'x', 100);
	TEST_ASSERT_EQUAL(0, memcmp(buffer, some_data, 100));
}

int push_some_tx_data(void)
{
	char data[100];
	memset(data, 'x', 100);
	return sep_write(sep, data, 100);
}

static char transmit_buffer[TX_BUF_SIZE];
int transmit_callback(void* data, int size)
{
	memcpy(transmit_buffer, data, size);
	return size;
}

TEST(serial, test_tx_data)
{
	//test in initial
	TEST_ASSERT_EQUAL(100, push_some_tx_data());

	//try to push new tx data before the previous one completed
	TEST_ASSERT_EQUAL(0, push_some_tx_data());

	//requested data correctness test
	char data[100];
	memset(data, 'x', 100);
	TEST_ASSERT_EQUAL(0, strncmp(transmit_buffer, data, 100));

	//set tx complete
	sep_tx_completed(sep);

	//try to push new tx data
	TEST_ASSERT_EQUAL(100, push_some_tx_data());

}
