/*
 * main.c
 *
 *  Created on: Feb 12, 2020
 *      Author: burak.kirazli
 */
#include "unity_fixture.h"

TEST_GROUP_RUNNER(serial)
{
	RUN_TEST_CASE(serial, test_create);
	RUN_TEST_CASE(serial, test_set_timeout);
	RUN_TEST_CASE(serial, test_tick_for_empty_rx_circ);
	RUN_TEST_CASE(serial, test_tick_for_non_empty_rx_circ);
	RUN_TEST_CASE(serial, test_tick_after_timeout);
	RUN_TEST_CASE(serial, test_rx_data);
	RUN_TEST_CASE(serial, test_tx_data);
}

void runner(void)
{
	RUN_TEST_GROUP(serial);
}

int main(int argc, const char* argv[])
{
	return UnityMain(argc, argv, runner);
}

